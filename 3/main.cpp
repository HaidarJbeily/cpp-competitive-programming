#include <bits/stdc++.h>


using namespace std;

const int N = 1000000;
string s;
int t = 0;
vector<string> v[N];

bool dfs(int depth){

	t = max(t, depth);

	getline(cin, s, ',');
	if(s == "") return false;

	v[depth].push_back(s);

	int l;
	cin >> l;
	cin.ignore();
	for(int i = 0; i < l; ++i)
		dfs(depth+1);

	return true;
}

int main(){

	while(dfs(0));

	cout << t+1 << endl;
	for(int i = 0; i <= t; ++i){
		for(int j = 0; j < v[i].size(); ++j)
			cout << v[i][j] << " ";

		cout << endl;
	}

	return 0;
}
