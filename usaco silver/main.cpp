#include <bits/stdc++.h>
using namespace std;
#define MAXN 100000
#define MAXB 100000

int n,m;
int dep[MAXN];
int did[MAXN];

bool d(int a,int b)
{
	return dep[a]<dep[b];
}

int snow[MAXB], dist[MAXB];
int ans[MAXB];
int bid[MAXB];

bool s(int a,int b)
{
	return snow[a] < snow[b];
}

int nxt[MAXN];
int pre[MAXN];

int main()
{
	cin >> n >> m;
	for(int i=0;i<n;i++)
	{
		cin >> dep[i];
		did[i] = i;
	}
	for(int i=0;i<m;i++)
	{
		cin >> snow[i] >> dist[i];
		bid[i] = i;
	}
	sort(did,did+n,d);
	sort(bid,bid+m,s);
	for(int i=0;i<N;i++)
		nxt[i] = i+1, pre[i] = i-1;
	int j = N-1;
	int maxStep = 1;
	for(int i=B-1;i>=0;i--)
	{
		int boot = bid[i];
		while(j >= 0 && dep[did[j]] > snow[boot])
		{
			int cur = did[j];
			nxt[pre[cur]] = nxt[cur];
			pre[nxt[cur]] = pre[cur];
			maxStep = max(maxStep, nxt[cur] - pre[cur]);
			j--;
		}
		ans[boot] = (maxStep <= dist[boot]);
	}
	for(int i=0;i<B;i++)
		cout << ans[i] << '\n';


return 0;
}
