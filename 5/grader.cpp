#include <bits/stdc++.h>
#include "AlphaHouse.h"
using namespace std;
long long AlphaHouse (int n , vector <string > v)
{
    long long ans=0;
    for (int i=0;i<n;i++)
    {
        for (int j=i+1;j<n;j++)
        {
            if (v[i]<v[j])ans++;
        }
    }
    return ans;
}
int main(){
	int n;
	cin>>n;
	vector<string>v;
	for(int i=0;i<n;i++){
		string s;
		cin>>s;
		v.push_back(s);
	}
	long long ans = AlphaHouse(n,v);
	cout<<ans<<endl;
}
