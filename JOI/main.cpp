#include <bits/stdc++.h>

using namespace std;
#define ll long long
const int MX=1e5+9;
int n,m;
vector < pair <int ,int > >v;
char a[500][500];
int dx[]={1,-1,0,0};
int dy[]={0,0,1,-1};
int main()
{
    cin >>n>>m;
   for (int i=0;i<n;i++)
    {
        for (int j=0;j<m;j++)
        {
            cin >>a[i][j];
            if (a[i][j]=='S')v.push_back({i,j});
        }

    }
    int z=0;
    for (int i=0;i<v.size();i++)
    {
        int s,t;
        s=v[i].first;
        t=v[i].second ;
        for (int j=0;j<4;j++)
        {
            int nx=s+dx[j];
            int ny=t+dy[j];
            if (nx >=n || nx<0 ||ny >=m || ny<0 )continue;
            if (a[nx][ny]=='W')z++;
            else if (a[nx][ny]=='.')a[nx][ny]='D';
        }
    }
    if (z)
    {
        cout <<"No";
        return 0;
    }
    else
    {
        cout <<"Yes \n";
        for (int i=0;i<n;i++)
        {
            for (int j=0;j<m;j++)
            {
                cout <<a[i][j];
            }
            cout <<"\n";
        }
    }

    return 0;
}
