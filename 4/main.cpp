/*
ID: mnmassr1
PROG: game1
LANG: C++
*/
#include <bits/stdc++.h>

using namespace std;
const int N=1e2+9;
vector<int >v;
int n;
int dp[N][N],pre[N][N];
int main()
{
    ofstream fout("game1.out");
    ifstream fin("game1.in");
    fin >>n;
    for (int i=0;i<n;i++)
    {
       fin>>dp[i][i];
       pre[i][i]=dp[i][i];
    }
    for (int i=0;i<n;i++)
    {
        for (int j=i+1;j<n;j++)
        {
            pre[i][j]=pre[i][j-1]+dp[j][j];
        }
    }
    for (int i=0;i<n;i++)
    {
        for (int j=0;j<n;j++)
        {
            dp[j][i+j]=pre[j][i+j]-min(dp[j][i+j-1],dp[j+1][j+i]);
        }
    }

    fout << dp[0][n-1]<<" "<<pre[0][n-1]-dp[0][n-1]<<"\n";
    return 0;
}
