#include <bits/stdc++.h>
#include "GOT.h"
using namespace std;
int dx[]= {0,0,-1,1};
int dy[]= {1,-1,0,0};
string s[1009];
int vis[1009][1009];
int it[1009][1009];
int GameOfThrones(int n,int m,vector <int >socr, vector <int >socc,vector <int >r, vector <int >c, vector <int>t )
{
        int  ans=0;

        queue <pair <int ,int > > q;
         q.push({n-1,m-1});

        memset( vis,0,sizeof vis);

        while (!q.empty())
        {
            pair <int ,int  >s1=q.front();
            q.pop();
            if (vis[s1.first ][s1.second  ]!=0)continue;
            vis[s1.first ][s1.second  ]=1;

            for (int i=0; i<4; i++)
            {

                int nx=s1.first+dx[i];
                int ny=s1.second+dy[i];
                if (ny>=m || ny < 0 || nx>=n || nx < 0 || s[nx][ny]== 'X' )continue;
                q.push({ nx,ny  });
               if (it[nx][ny]==0) it[nx][ny]=it[s1.first][s1.second]+1;

            }


        }
        for (int i=0;i<r.size();i++)
        {
            if (it[r[i]][c[i]  ]<=t[i]){ans++;            }
        }

    return ans;



}
