#include <bits/stdc++.h>
#include "GOT.h"
using namespace std;
int dx[]= {0,0,-1,1};
int dy[]= {1,-1,0,0};
string s[1009];
int vis[1009][1009];
int it[1009][1009];
int GameOfThrones(int n,int m,vector <int >socr, vector <int >socc,vector <int >r, vector <int >c, vector <int>t )
{
        int  ans=0;

        queue <pair <int ,int > > q;
         q.push({n-1,m-1});

        memset( vis,0,sizeof vis);

        while (!q.empty())
        {
            pair <int ,int  >s1=q.front();
            q.pop();
            if (vis[s1.first ][s1.second  ]!=0)continue;
            vis[s1.first ][s1.second  ]=1;

            for (int i=0; i<4; i++)
            {

                int nx=s1.first+dx[i];
                int ny=s1.second+dy[i];
                if (ny>=m || ny < 0 || nx>=n || nx < 0 || s[nx][ny]== 'X' )continue;
                q.push({ nx,ny  });
               if (it[nx][ny]==0) it[nx][ny]=it[s1.first][s1.second]+1;

            }


        }
        for (int i=0;i<r.size();i++)
        {
            if (it[r[i]-1][c[i]-1  ]<=t[i]){ans++;            }
        }

    return ans;



}

int main(){
	int N,M,R;

	cin>>N>>M>>R;
	for(int i=0;i<N;i++){
		cin>>s[i];
	}
	vector<int>Rose_R;
	vector<int>Rose_C;
	vector<int>Rose_T;
	for(int i=0;i<R;i++){
		int x,y,t;
		cin>>x>>y>>t;
		Rose_R.push_back(x);
		Rose_C.push_back(y);
		Rose_T.push_back(t);
	}
	vector<int>Ob_R;
	vector<int>Ob_C;
	for(int i=0;i<N;i++){
		for(int j=0;j<M;j++){
			if(s[i][j] == 'X'){
				Ob_R.push_back(i);
				Ob_C.push_back(j);
			}
		}
	}
	cout <<"\n";
	int Ans = GameOfThrones(N,M,Ob_R,Ob_C,Rose_R,Rose_C,Rose_T);
	cout<<Ans<<endl;
	return  0;
}
/*
20 20 5
.X....
....X.
X.....
X..X..
.X....
....X.
X.....
X..X..
.X....
....X.
X.....
X..X..
X..X..
X..X..
X..X..
X..X..
X..X..
X..X..
X..X..
2 2 2
3 3 4
4 2 6
3 4 1
3 4 2


*/
/*
2 5 18
...X.
.....
1 3 3
1 1 4
1 1 3
0 2 1
0 0 2
1 2 4
1 2 5
0 0 4
0 2 3
1 1 6
1 2 6
0 0 4
1 4 3
0 1 4
0 4 3
0 0 6
0 0 2
0 0 4




*/
