#include <bits/stdc++.h>
#include <iostream>
#include <limits.h>
#include <string.h>
#include <queue>
using namespace std;


#define V 400
 int graph[400][400];
bool bfs(int rGraph[V][V], int s, int t, int parent[])
{

    bool visited[V];
    memset(visited, 0, sizeof(visited));

    queue <int> q;
    q.push(s);
    visited[s] = true;
    parent[s] = -1;


    while (!q.empty())
    {
        int u = q.front();
        q.pop();

        for (int v=0; v<V; v++)
        {


            if (visited[v]==false && rGraph[u][v] > 0)
            {
                q.push(v);
                parent[v] = u;
                visited[v] = true;
            }
        }
    }


    return (visited[t] == true);
}

int fordFulkerson( int s, int t)
{
    int u, v;

    int rGraph[V][V];
    for (u = 0; u < V; u++)
        for (v = 0; v < V; v++)
             rGraph[u][v] = graph[u][v];

    int parent[V];

    int max_flow = 0;
    while (bfs(rGraph, s, t, parent))
    {
        int path_flow = INT_MAX;
        for (v=t; v!=s; v=parent[v])
        {
            u = parent[v];
            path_flow = min(path_flow, rGraph[u][v]);
            cout <<path_flow<<endl;
        }

        for (v=t; v != s; v=parent[v])
        {
            u = parent[v];
            rGraph[u][v] -= path_flow;
            rGraph[v][u] += path_flow;
        }


        max_flow += path_flow;
    }


    return max_flow;
}
pair <int , int > pe[400],ta[400];
int t1,p,t,s,c;
int main()
{
   scanf ("%d",&t1);
   while (t1--)
   {
       memset(graph , 0 ,sizeof graph );
       cin >>p>>t>>s>>c;
       for (int i=0;i<p;i++)
            cin >>pe[i].first>>pe[i].second;
    for (int i=0;i<t;i++)
            cin >>pe[i].first>>pe[i].second;

       for (int i=0;i<p;i++)
       {
           for (int j=0;j<t;j++)
           {
               int x=abs(pe[i].first-ta[j].first);
               int y=abs(pe[i].second-ta[j].second);
               int dist=200*(x+y);
               double  chk=dist/s;
               if (chk<=c)
               {
                graph[i][j]=1;
               }
           }
       }
       cout <<fordFulkerson(0 ,400);
   }




    return 0;
}
