#include <iostream>

using namespace std;
#define ll long long

ll gcd(ll x, ll y)
{
    if (!y)return x;
    gcd(y,x%y);
}
ll lcm (ll x, ll y)
{
    return (x*y)/gcd(x,y);
}


int main()
{
    ll x1,x2;
    cin >>x1 >> x2;
    cout <<lcm(x1,x2);

    return 0;
}
