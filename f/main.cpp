//DIJKSTRA CODE SHORTEST PATH
#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> pi;
const int MAX = 100001;
bool visited[MAX];
int distanc[MAX];
vector<vector<pi> > G;
// assume weighted graph is stored in adjacency List ( vectors )
// Dijkstra Algorithm to find Shortest Path from Start to Goal
int Dijkstra(int start, int Goal)
{
    memset(visited, 0, sizeof visited);
    memset(distanc, -1, sizeof distanc);
    priority_queue< pi, vector<pi>, greater<pi> > Q;
    Q.push(make_pair(0, start));
    while (!Q.empty())
    {
        pi p = Q.top();
        Q.pop();
        int node = p.second, dist = p.first;
        if (visited[node]) 			continue;
        distanc[node] = dist;
        visited[node] = true;
        if (node == Goal) // Goal reached
            return dist;
        for (int i = 0; i < G[node].size(); ++i)
        {
            int child = G[node][i].first ;
            int edge = G[node][i].second;
            if (!visited[child]) Q.push(make_pair(dist + edge, child ));
        }
    }
    return -1; // if there is no path !
}
