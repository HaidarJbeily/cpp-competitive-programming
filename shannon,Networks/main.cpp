#include <bits/stdc++.h>

using namespace std;

struct node
{
    char sym[10];
    float pro;
    int arr[20];
    int top;

}s[20];
typedef struct node node;
void shannon(int l , int h , node s[])
{
    float pack1=0,pack2=0,diff1=0,diff2=0;
    int i,d,k,j;
    if ( (l+1)==h || l==h || l>h  )
    {
        if (l==h || l>h)
        return;
        s[h].arr[++(s[h].top)]=0;
        s[l].arr[++(s[l].top)]=1;
        return;
    }
    else
    {
        for (i=l;i<h;i++)
          pack1=pack1+s[i].pro;
          pack2+=s[h].pro;
          diff1=pack1-pack2;
          if (diff1<0)diff1*=-1;
          j=2;
          while (j!=h-l+1)
          {
              k=h-j;
              pack1=pack2=0;
              for (i=l;i<=k;i++)
              {
                  pack1+=s[i].pro;
              }
              for (int i=h;i>k;i--)
              {
                  pack2+=s[i].pro;
              }
              diff2=pack1-pack2;
              if (diff2<0)diff2*=-1;
              if (diff2>=diff1)break;
              diff1=diff2;
              j++;
          }
          k++;
          for (i=l;i<=k;i++)
          {
              s[i].arr[++(s[i].top)]=1;

          }
          for (int i=k+1;i<=h;i++)
          {
              s[i].arr[++(s[i].top)]=0;
          }
          shannon(l,k,s);
          shannon(k+1,h,s);
    }
}
int main()
{
    int i , j ,n;
    float x,total=0;
    char ch[10];
    node temp;
    cout <<"Enter how Many Sympols Do you want To Enter \t: ";
    cin >>n;
    for (int i =0 ;i<n;i++)
    {
        cout <<"Enter Sympol ----> "<<i+1;
        cin >>ch;
        strcpy(s[i].sym,ch);

    }
    for (i=0;i<n;i++)
    {
        cout <<"Enter probability for ---> "<<s[i].sym;
        cin >>x;
        s[i].pro=x;
        total+=x;
        if (total>1)
        {
            cout <<"This probability is not possible , Enter new probability";
            total-=x;
            i--;
        }
    }
    s[i].pro=1-total;
    for (int j=1;j<n;j++)
    {
        for (int i=0;i<n-1;i++)
        {
            if ((s[i].pro)>(s[i+1].pro ))
            {
                temp.pro=s[i].pro;
                strcpy(temp.sym,s[i].sym);
                s[i].pro=s[i+1].pro;
                strcpy(s[i].sym,s[i+1].sym);
                s[i+1].pro=temp.pro;
                strcpy(s[i+1].sym,temp.sym);
            }
        }
    }
   for (i=0;i<n;i++)
    s[i].top=-1;


   shannon(0,n-1,s);

   cout <<"\n----------------------------------------\n";
     cout <<"Symbol\tPrbability\tCode\n";
     for (int i=n-1;i>=0;i--)
     {
         cout <<s[i].sym<<" "<<s[i].pro<<" ";
         for (int j=0;j<=s[i].top;j++)
         {
             cout <<s[i].arr[j];
         }
         cout <<endl;
     }




    return 0;
}
