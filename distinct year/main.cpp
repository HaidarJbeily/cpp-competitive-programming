#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n;
    cin >>n;
    while (n++)
    {
        vector <int >v;
        int z=n;
        while (z)
        {
            v.push_back(z%10);
            z/=10;
        }
        int chk=0;
        for (int i=0;i<v.size();i++)
        {
            for (int j=i+1;j<v.size();j++)
            {
                if (v[i]==v[j])
                {
                    chk=1;
                }
            }
        }

        if (!chk)
        {
            cout <<n;
            return 0;
        }

    }
    return 0;
}
